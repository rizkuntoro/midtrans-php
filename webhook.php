<?php
 
require_once(dirname(__FILE__) . '/Midtrans.php');
\Midtrans\Config::$isProduction = false;
\Midtrans\Config::$serverKey = '<ENTER SERVER KEY>';
$notif = new \Midtrans\Notification();
 
$transaction = $notif->transaction_status;
$type = $notif->payment_type;
$order_id = $notif->order_id;
$fraud = $notif->fraud_status;
$gross_amount = substr($notif->gross_amount, 0, -3); // DELETE DECIMAL 
 
if ($transaction == 'capture') {
  // For credit card transaction, we need to check whether transaction is challenge by FDS or not
  if ($type == 'credit_card'){
    if($fraud == 'challenge'){
      // TODO set payment status in merchant's database to 'Challenge by FDS'
      // TODO merchant should decide whether this transaction is authorized or not in MAP
      $detail = "Transaction order_id: " . $order_id ." is challenged by FDS";
      }
      else {
      // TODO set payment status in merchant's database to 'Success'
      $detail = "Transaction order_id: " . $order_id ." successfully captured using " . $type;
      }
    }
  }
else if ($transaction == 'settlement'){
  // TODO set payment status in merchant's database to 'Settlement'
  $detail = "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
  }
  else if($transaction == 'pending'){
  // TODO set payment status in merchant's database to 'Pending'
  $detail = "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
  }
  else if ($transaction == 'deny') {
  // TODO set payment status in merchant's database to 'Denied'
  $detail = "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
  }
  else if ($transaction == 'expire') {
  // TODO set payment status in merchant's database to 'expire'
  $detail = "Payment using " . $type . " for transaction order_id: " . $order_id . " is expired.";
  }
  else if ($transaction == 'cancel') {
  // TODO set payment status in merchant's database to 'Denied'
  $detail = "Payment using " . $type . " for transaction order_id: " . $order_id . " is canceled.";
}

/* UPDATE DATA TRANSAKSI */

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "midtrans";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}


$data = mysqli_query($conn,"SELECT * FROM transaksi WHERE `id` = '$order_id' AND `total_harga` = '$gross_amount'");
if (mysqli_num_rows($data) > 0) {
    mysqli_query($conn,"
        UPDATE transaksi 
        set status='".$transaction."', detail='".$detail."', json='".json_encode($notif)."'
        WHERE id='".$order_id."'");
}

header("HTTP/1.1 200 OK");

$conn->close();
